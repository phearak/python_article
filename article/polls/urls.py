from django.urls import path

from . import views

app_name = 'article'
urlpatterns = [
    # ex: article/
    path('', views.IndexView.as_view(), name='index'),
    # ex: article/5/
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    # ex: article/5/results/
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # ex: article/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote')
]
